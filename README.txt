CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides Payment module integration with Authorize.Net's "Accept
Hosted" payment method; a mobile-optimized payment form hosted by Authorize.Net
that enables developers to use the Authorize.Net API to submit payment
transactions, while maintaining SAQ-A level PCI compliance.

REQUIREMENTS
------------

Before you can install this module, you will need:

1. Composer (https://getcomposer.org/) or an equivalent,
2. Drupal 8.3.0 or higher, running on PHP 5.6.0 or higher, and,
3. The Payment module (https://www.drupal.org/project/payment), 8.x-2.0-rc3 or
   higher, and its dependencies.

INSTALLATION
------------

1. Install and configure the requirements, including all of the modules which
   Payment depends on. Please see the requirements' READMEs and documentation
   for further information.
2. Install this module as you would normally install a contributed Drupal 8
   module. Visit: https://www.drupal.org/docs/user_guide/en/config-install.html
   for further information.
    a. If you did NOT install this module using Composer, you will probably need
       to install the Official PHP SDK for Authorize.Net, version ~1.9. You can
       do this with composer by running:

              composer require 'authorizenet/authorizenet:~1.9'

CONFIGURATION
-------------

1. Go to /admin/config/services/payment/method
2. You will see a list of payment methods. Click the "Add payment method
   configuration" button.
3. Click "Authorize.Net Accept Hosted".
4. At minimum, you must enter information for the following fields:
    a. "Label": a name that will be displayed to end users if more than one
       payment method is enabled.
    b. "Machine-readable name" ("Machine name"): Drupal's internal name for the
       payment method. This is usually generated automatically by Drupal's admin
       UI based upon what you enter in the "Label" field.

       Take note of this machine-readable name if you wish to override settings
       for this module in your `settings.php` or `settings.local.php` files. See
       below for more details.

    c. "Owner": a reference to the user which owns the configuration (the
       default is usually fine).
    d. Under "Authorize.Net API connection settings":
        1. "Authorize.Net environment": which Authorize.Net environment (mode)
           to send requests to (Sandbox is for testing so you will not collect
           any money - Production is for general use).
        2. "Form submit URL": which Authorize.Net environment (mode) will serve
           the Accept Hosted payment form. This **must** match "Authorize.Net
           environment" above to work properly!
        3. "API Login ID": a machine name that identifies you, kind-of like a
           username. This is **NOT** the username you use to log into your
           Authorize.Net account! See the field description for instructions on
           how to find your API Login ID.
        4. "Merchant's unique Transaction Key": a key that identifies you, kind-
           of like a password. This is **NOT** the username you use to log into
           your Authorize.Net account! See the field description for
           instructions on how to find your Transaction Key.
    e. Under "Authorize.Net transaction request settings":
        1. "Type of credit card transaction": The type of Accept Hosted payment
           form to request. "Authorize and capture" will reserve the funds on
           the credit card, AND bill the credit card in the same step, and is
           likely what you want. "Authorize" will just reserve the funds on the
           credit card; if you do not "Capture" the funds in a separate step
           within a certain period of time (30 days), the funds will be
           released.

Once you have saved a payment method, you can override its settings in your
`settings.php` or `settings.local.php` files. This is useful for development
teams with local environments, as well as development / testing / staging
servers, where you want to test without making real transactions with your own,
real money, and/or use your own personal credentials so that *you* receive the
emails that Authorize.Net sends out instead of your client.

To override the settings for a payment method, you need to know its machine-
readable name, as well as the names and values for the settings you want to
override. The full list of configuration setting names are listed in the
function named `defaultConfiguration()` in
`src/Plugin/Payment/MethodConfiguration/AuthorizeNetAcceptHosted.php`; but you
can also look at the names of the fields in the HTML for the configuration page.

The following snippet overrides the "Authorize.Net environment", "Form submit
URL", "API Login ID", and "Merchant's unique Transaction Key" for the payment
method with the machine name `MACH_NAME`:

```
// Payment: Authorize.net Accept Hosted settings.
$config['payment.payment_method_configuration.MACH_NAME']['pluginConfiguration']['environment'] = 'https://apitest.authorize.net';
$config['payment.payment_method_configuration.MACH_NAME']['pluginConfiguration']['form_submit_url'] = 'https://test.authorize.net/payment/payment';
$config['payment.payment_method_configuration.MACH_NAME']['pluginConfiguration']['merchantAuthentication_name'] = '12345abcde';
$config['payment.payment_method_configuration.MACH_NAME']['pluginConfiguration']['merchantAuthentication_transactionKey'] = '12345abcdeABCDEF';
```

MAINTAINERS
-----------

Current maintainers:

 * M Parker - https://www.drupal.org/u/mparker17
 * drsmallwood - https://www.drupal.org/u/drsmallwood

This project has been sponsored by:

 * Cool Blue Interactive - https://www.drupal.org/cool-blue-interactive
