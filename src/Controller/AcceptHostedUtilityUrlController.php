<?php

namespace Drupal\payment_authnet_accepthosted\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Router _controller callbacks for Authorize.Net AH "utility" URLs.
 */
class AcceptHostedUtilityUrlController {

  /**
   * Page callback: Display a page for the Authorize.Net AH "Continue" URL.
   *
   * A "Continue" URL needs to be provided, even if the user will never access
   * it and/or it is not valid given that we displaying the Authorize.Net Accept
   * Hosted page in an iframe. If a "Continue" URL is not provided,
   * Authorize.Net will never pass a transactResponse message back to the
   * IFrameCommunicator.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An HTTP 204 No Content response.
   *
   * @see payment.authnet_accepthosted.continue_callback
   * @see \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetAcceptHostedFormTokenFactory::buildSettings
   */
  public function executeContinueUrl() {
    return new Response('', Response::HTTP_NO_CONTENT);
  }

  /**
   * Page callback: Display a page for the Authorize.Net AH "Cancel" URL.
   *
   * A "Cancel" URL needs to be provided, even if the user will never access
   * it and/or it is not valid given that we displaying the Authorize.Net Accept
   * Hosted page in an iframe. If a "Cancel" URL is not provided, Authorize.Net
   * will never pass a transactResponse message back to the IFrameCommunicator.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An HTTP 204 No Content response.
   *
   * @see payment.authnet_accepthosted.cancel_callback
   * @see \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetAcceptHostedFormTokenFactory::buildSettings
   */
  public function executeCancelUrl() {
    return new Response('', Response::HTTP_NO_CONTENT);
  }

}
