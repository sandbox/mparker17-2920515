<?php

namespace Drupal\payment_authnet_accepthosted\Exception;

/**
 * An exception to indicate that the Authorize.Net API returned a NULL response.
 */
class NullResponseException extends \RuntimeException {}
