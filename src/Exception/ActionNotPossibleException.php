<?php

namespace Drupal\payment_authnet_accepthosted\Exception;

/**
 * A type of exception to throw when the requested action is not possible.
 *
 * For example, trying to refund or void a transaction that has not been
 * started.
 */
class ActionNotPossibleException extends \RuntimeException {}
