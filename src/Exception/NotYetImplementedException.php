<?php

namespace Drupal\payment_authnet_accepthosted\Exception;

/**
 * An exception to throw when we have not yet implemented a feature.
 */
class NotYetImplementedException extends \LogicException {}
