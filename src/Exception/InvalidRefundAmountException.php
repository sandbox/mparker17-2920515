<?php

namespace Drupal\payment_authnet_accepthosted\Exception;

/**
 * A type of exception to throw when a refund amount was invalid.
 */
class InvalidRefundAmountException extends \RuntimeException {}
