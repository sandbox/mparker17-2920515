<?php

namespace Drupal\payment_authnet_accepthosted\Exception;

/**
 * An exception to indicate the Authorize.Net API returned a non-Ok response.
 */
class ResultCodeNotOkException extends \RuntimeException {

  /**
   * The result code that was not "Ok".
   *
   * @var string
   */
  protected $resultCode;

  /**
   * An array of message strings returned by the Authorize.Net API.
   *
   * @var string[]
   */
  protected $messages;

  /**
   * ResultCodeNotOkException constructor.
   *
   * @param string $message
   *   The Exception message to throw.
   * @param int $code
   *   The Exception code.
   * @param string $resultCode
   *   The result code from the Authorize.Net API.
   * @param array $messages
   *   The array of messages returned by the Authorize.Net API.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct($message = "", $code = 0, $resultCode = '', array $messages = [], \Throwable $previous = NULL) {
    $this->resultCode = $resultCode;
    $this->messages = $messages;

    $combined_messsage = $message
      . ' Result code was: ' . $resultCode
      . ' Messages were: ' . implode(', ', $messages);
    parent::__construct($combined_messsage, $code, $previous);
  }

  /**
   * Get the result code that was not "Ok".
   *
   * @return string
   *   The result code that was not "Ok".
   */
  public function getResultCode() {
    return $this->resultCode;
  }

  /**
   * Get the array of message strings returned by the Authorize.Net API.
   *
   * @return string[]
   *   An array of message strings returned by the Authorize.Net API.
   */
  public function getMessages() {
    return $this->messages;
  }

}
