<?php

namespace Drupal\payment_authnet_accepthosted;

use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1TransactionResponseCodes;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1TransactionStatuses;

/**
 * A class to interpret all the payment statuses and codes.
 */
class AuthorizeNetV1TransactionCodeAndStatusInterpreter {

  /**
   * Determine if a status counts as "successfully executed" (authorized).
   *
   * The Payment module's "execute" status is roughly equivalent to
   * Authorize.Net's "authorize" status.
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if the transaction with the given response code and transaction
   *   status can be interpreted as "successfully executed"; FALSE otherwise.
   */
  public static function inSuccessfulExecuteState($responseCode, $transactionStatus) {
    return ((int) $responseCode) === AuthorizeNetV1TransactionResponseCodes::APPROVED
      && (
        $transactionStatus === AuthorizeNetV1TransactionStatuses::AUTHORIZED_PENDING_CAPTURE
        // Even though the FDS has flagged this transaction for manual review,
        // it was confident enough that it is real to consider it "authorized".
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::FDS_AUTHORIZED_PENDING_REVIEW
      );
  }

  /**
   * Determine if a status counts as "successfully captured".
   *
   * The Payment module's "capture" status is roughly equivalent to
   * Authorize.Net's "capture" status.
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if the transaction with the given response code and transaction
   *   status can be interpreted as "successfully captured"; FALSE otherwise.
   */
  public static function inSuccessfulCaptureState($responseCode, $transactionStatus) {
    return ((int) $responseCode) === AuthorizeNetV1TransactionResponseCodes::APPROVED
      && (
        $transactionStatus === AuthorizeNetV1TransactionStatuses::CAPTURED_PENDING_SETTLEMENT
        // Note that settlement happens automatically, but it usually takes a
        // long time (a few days/weeks).
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::SETTLED_SUCCESSFULLY
      );
  }

  /**
   * Determine if a status counts as "sucessfully refunded".
   *
   * The Payment module's "refund" status is equivalent to both Authorize.Net's
   * "void" and "refund" statuses.
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if the transaction with the given response code and transaction
   *   status can be interpreted as "successfully refunded"; FALSE otherwise.
   */
  public static function inSuccessfulRefundState($responseCode, $transactionStatus) {
    return ((int) $responseCode) === AuthorizeNetV1TransactionResponseCodes::APPROVED
      && (
        $transactionStatus === AuthorizeNetV1TransactionStatuses::REFUND_PENDING_SETTLEMENT
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::REFUND_SETTLED_SUCCESSFULLY
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::VOIDED
      );
  }

  /**
   * Determine if a status counts as "declined".
   *
   * This generally means the payment cannot happen.
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if $testStatus can be interpreted as "declined"; FALSE otherwise.
   */
  public static function inDeclinedState($responseCode, $transactionStatus) {
    return ((int) $responseCode) === AuthorizeNetV1TransactionResponseCodes::DECLINED
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::DECLINED
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::FAILED_REVIEW;
  }

  /**
   * Determine if a status counts as "needs further manual action".
   *
   * Whether the transaction will succeed or not is indeterminate at this time.
   * You will need to manually flip a "let's try this" switch and wait to see
   * what happens.
   *
   * This can be considered equivalent to "declined" for transactions that will
   * be fulfilled immediately (e.g.: downloads) or automatically (i.e.: by
   * warehouse robots).
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if $testStatus can be interpreted as "needs further manual action";
   *   FALSE otherwise.
   */
  public static function inNeedsFurtherManualActionState($responseCode, $transactionStatus) {
    return ((int) $responseCode) === AuthorizeNetV1TransactionResponseCodes::HELD_FOR_REVIEW
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::FDS_AUTHORIZED_PENDING_REVIEW
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::FDS_PENDING_REVIEW
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::UNDER_REVIEW;
  }

  /**
   * Determine if a status counts as "voidable" (as opposed to "refundable").
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if the parameters can be interpreted as "voidable"; FALSE otherwise.
   */
  public static function isVoidable($responseCode, $transactionStatus) {
    return $responseCode !== AuthorizeNetV1TransactionResponseCodes::ERROR
      && (
        $transactionStatus === AuthorizeNetV1TransactionStatuses::AUTHORIZED_PENDING_CAPTURE
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::FDS_AUTHORIZED_PENDING_REVIEW
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::CAPTURED_PENDING_SETTLEMENT
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::REFUND_PENDING_SETTLEMENT
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::DECLINED
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::FAILED_REVIEW
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::FDS_AUTHORIZED_PENDING_REVIEW
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::FDS_PENDING_REVIEW
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::UNDER_REVIEW
      );
  }

  /**
   * Determine if a status counts as "refundable" (as opposed to "voidable").
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if the parameters can be interpreted as "refundable"; FALSE
   *   otherwise.
   */
  public static function isRefundable($responseCode, $transactionStatus) {
    return $responseCode !== AuthorizeNetV1TransactionResponseCodes::ERROR
      && (
        $transactionStatus === AuthorizeNetV1TransactionStatuses::SETTLED_SUCCESSFULLY
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::REFUND_SETTLED_SUCCESSFULLY
      );
  }

  /**
   * Determine if a status counts as "in an error state".
   *
   * Whether the transaction will succeed or not is indeterminate at this time.
   * You will need to retry in a medium-to-long time (hours, days, weeks) and/or
   * contact Authorize.Net support.
   *
   * This can be considered equivalent to "declined" for transactions that will
   * be fulfilled immediately (e.g.: downloads) or automatically (i.e.: by
   * warehouse robots).
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if $testStatus can be interpreted as "in an error state"; FALSE
   *   otherwise.
   */
  public static function inErrorState($responseCode, $transactionStatus) {
    return ((int) $responseCode) === AuthorizeNetV1TransactionResponseCodes::ERROR
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::EXPIRED
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::COMMUNICATION_ERROR
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::GENERAL_ERROR
      || $transactionStatus === AuthorizeNetV1TransactionStatuses::SETTLEMENT_ERROR;
  }

  /**
   * Determine if a status counts as "in a transitional state".
   *
   * This essentially means, "I'm procssing this as we speak; ask me again in a
   * short time" (seconds). Reportedly, encountering these statuses is very
   * rare.
   *
   * Try asking for the state again immediately after recieving this.
   * Transactions that will be fulfilled immediately (e.g.: downloads) or
   * automatically (i.e.: by warehouse robots) can consider this "declined" if
   * they've received this status at least twice in a row.
   *
   * @param string|int $responseCode
   *   A response code to check.
   * @param string $transactionStatus
   *   A status string to check.
   *
   * @return bool
   *   TRUE if $testStatus can be interpreted as "in a transitional state";
   *   FALSE otherwise.
   */
  public static function inTransitionalState($responseCode, $transactionStatus) {
    return !is_null($responseCode)
      && (
        $transactionStatus === AuthorizeNetV1TransactionStatuses::APPROVED_REVIEW
        || $transactionStatus === AuthorizeNetV1TransactionStatuses::COULD_NOT_VOID
      );
  }

}
