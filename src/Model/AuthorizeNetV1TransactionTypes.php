<?php

namespace Drupal\payment_authnet_accepthosted\Model;

/**
 * A class to enumerate Authorize.Net API version 1 transaction types.
 *
 * Apparently the Authorize.Net PHP API does not enumerate these.
 *
 * @see https://developer.authorize.net/api/reference/#payment-transactions-get-an-accept-payment-page
 */
class AuthorizeNetV1TransactionTypes {

  /**
   * A transaction that involves both authorizing and capturing the money.
   *
   * @var string
   */
  const AUTH_CAPTURE_TRANSACTION = 'authCaptureTransaction';

  /**
   * A transaction that involves only authorizing the money.
   *
   * @var string
   */
  const AUTH_ONLY_TRANSACTION = 'authOnlyTransaction';

  /**
   * A transaction to refund a previous transaction.
   *
   * @var string
   */
  const REFUND_TRANSACTION = 'refundTransaction';

  /**
   * A transaction to void a previous transaction.
   *
   * @var string
   */
  const VOID_TRANSACTION = 'voidTransaction';

}
