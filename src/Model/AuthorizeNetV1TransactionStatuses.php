<?php

namespace Drupal\payment_authnet_accepthosted\Model;

/**
 * A class to enumerate transaction statuses.
 */
class AuthorizeNetV1TransactionStatuses {

  /* BasicTransaction Statuses. */

  /**
   * The payment was authorized but still needs to be captured.
   *
   * Capturing the transaction will have to be done manually.
   *
   * @var string
   */
  const AUTHORIZED_PENDING_CAPTURE = 'authorizedPendingCapture';

  /**
   * The payment was captured but still needs to be settled.
   *
   * The settlement should happen automatically.
   *
   * @var string
   */
  const CAPTURED_PENDING_SETTLEMENT = 'capturedPendingSettlement';

  /**
   * The transaction was declined.
   *
   * @var string
   */
  const DECLINED = 'declined';

  /**
   * The transaction has expired.
   *
   * This might occur if you authorized the payment but did not capture it
   * within 30 days.
   *
   * @var string
   */
  const EXPIRED = 'expired';

  /**
   * The refund is pending settlement.
   *
   * The settlement should happen automatically.
   *
   * @var string
   */
  const REFUND_PENDING_SETTLEMENT = 'refundPendingSettlement';

  /**
   * The refund was settled successfully.
   *
   * This means the credit from the refund has been applied to the user's credit
   * card account.
   *
   * @var string
   */
  const REFUND_SETTLED_SUCCESSFULLY = 'refundSettledSuccessfully';

  /**
   * The transaction has been settled.
   *
   * The money has been transferred.
   *
   * @var string
   */
  const SETTLED_SUCCESSFULLY = 'settledSuccessfully';

  /**
   * The transaction was successfully voided.
   *
   * @var string
   */
  const VOIDED = 'voided';

  /* Fraud Detection Suite (FDS or AFDS) Specific Responses. */

  /**
   * Fraud Detection Suite: Authorized; but needs merchant to review manually.
   *
   * @var string
   */
  const FDS_AUTHORIZED_PENDING_REVIEW = 'FDSAuthorizedPendingReview';

  /**
   * Fraud Detection Suite: Not authorized; needs merchant to review manually.
   *
   * @var string
   */
  const FDS_PENDING_REVIEW = 'FDSPendingReview';

  /* eCheck Specific Responses. */

  /**
   * eCheck: The transaction failed review.
   *
   * @var string
   */
  const FAILED_REVIEW = 'failedReview';

  /**
   * eCheck: The item was returned.
   *
   * Not sure what this means.
   *
   * @var string
   */
  const RETURNED_ITEM = 'returnedItem';

  /**
   * eCheck: The transaction is under review.
   *
   * @var string
   */
  const UNDER_REVIEW = 'underReview';

  /* Other Errors. */

  /**
   * Other: an individual transaction was rejected by the processor.
   *
   * This is a final transaction status.
   *
   * @var string
   */
  const COMMUNICATION_ERROR = 'communicationError';

  /**
   * Other: There was an unspecified error.
   *
   * Recommend contacting Authorize.Net to find out what's up with that.
   *
   * @var string
   */
  const GENERAL_ERROR = 'generalError';

  /**
   * Other: A day's batch was rejected by the processor.
   *
   * This status is not final. The merchant should try to recover the batch.
   *
   * @var string
   */
  const SETTLEMENT_ERROR = 'settlementError';

  /* Transitional Transaction Statuses. */

  /**
   * Transitional: Occurs as transaction review is approved.
   *
   * Check back in a few minutes for an updated status.
   *
   * @var string
   */
  const APPROVED_REVIEW = 'approvedReview';

  /**
   * Transitional: The transaction could not be voided.
   *
   * Check back in a few minutes for an updated status.
   *
   * @var string
   */
  const COULD_NOT_VOID = 'couldNotVoid';

}
