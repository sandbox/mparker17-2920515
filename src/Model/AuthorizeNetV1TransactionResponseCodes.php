<?php

namespace Drupal\payment_authnet_accepthosted\Model;

/**
 * A class to enumerate transaction responseCodes.
 *
 * Apparently the Authorize.Net PHP API does not enumerate these.
 *
 * @see https://developer.authorize.net/api/reference/index.html#transaction-reporting-get-transaction-details
 */
class AuthorizeNetV1TransactionResponseCodes {

  /**
   * The transaction was approved.
   *
   * @var string
   */
  const APPROVED = 1;

  /**
   * The transaction was declined.
   *
   * @var string
   */
  const DECLINED = 2;

  /**
   * The transaction encountered an error.
   *
   * @var string
   */
  const ERROR = 3;

  /**
   * The transaction was held for review.
   *
   * @var string
   */
  const HELD_FOR_REVIEW = 4;

}
