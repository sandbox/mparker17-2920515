<?php

namespace Drupal\payment_authnet_accepthosted\Model;

/**
 * A class to enumerate Authorize.Net API version 1 result codes.
 *
 * Apparently the Authorize.Net PHP API does not enumerate these.
 *
 * @see https://developer.authorize.net/api/reference/#payment-transactions-get-an-accept-payment-page
 */
class AuthorizeNetV1ResultCodes {

  /**
   * A result code indicating the API call was successful.
   *
   * @var string
   */
  const OK = 'Ok';

}
