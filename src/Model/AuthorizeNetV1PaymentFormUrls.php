<?php

namespace Drupal\payment_authnet_accepthosted\Model;

/**
 * A class to enumerate Authorize.Net API version 1 Accept Hosted submit URLs.
 *
 * Apparently the Authorize.Net PHP API does not enumerate these.
 *
 * @see http://developer.authorize.net/api/reference/features/accept_hosted.html#Displaying_the_Form
 */
class AuthorizeNetV1PaymentFormUrls {

  /**
   * The URL of the sandbox payment form.
   *
   * @var string
   */
  const SANDBOX = 'https://test.authorize.net/payment/payment';

  /**
   * The URL of the production payment form.
   *
   * @var string
   */
  const PRODUCTION = 'https://accept.authorize.net/payment/payment';

}
