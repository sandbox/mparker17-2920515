<?php

namespace Drupal\payment_authnet_accepthosted\Model;

use net\authorize\api\contract\v1\TransactionDetailsType;

/**
 * A container class to hold data returned from a transaction request.
 */
class TransactionUpdateInformation {

  /**
   * The transaction returned by the Authorize.Net API.
   *
   * @var \net\authorize\api\contract\v1\TransactionDetailsType
   */
  protected $transaction;

  /**
   * The UNIX time right after the transaction returned to Drupal successfully.
   *
   * @var int
   */
  protected $lastUpdate;

  /**
   * TransactionUpdateInformation constructor.
   *
   * @param \net\authorize\api\contract\v1\TransactionDetailsType $transaction
   *   The transaction returned by the Authorize.Net API.
   * @param int $lastUpdate
   *   The UNIX time right after the transaction returned to Drupal
   *   successfully.
   */
  public function __construct(TransactionDetailsType $transaction, $lastUpdate) {
    $this->transaction = $transaction;
    $this->lastUpdate = $lastUpdate;
  }

  /**
   * Get the transaction returned by the Authorize.Net API.
   *
   * @return \net\authorize\api\contract\v1\TransactionDetailsType
   *   The transaction returned by the Authorize.Net API.
   */
  public function getTransaction() {
    return $this->transaction;
  }

  /**
   * Get the UNIX time after the transaction returned to Drupal successfully.
   *
   * @return int
   *   The UNIX time right after the transaction returned to Drupal
   *   successfully.
   */
  public function getLastUpdate() {
    return $this->lastUpdate;
  }

}
