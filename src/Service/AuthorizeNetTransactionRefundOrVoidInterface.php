<?php

namespace Drupal\payment_authnet_accepthosted\Service;

use net\authorize\api\contract\v1\TransactionDetailsType;

/**
 * Refund or void an Authorize.Net transaction.
 */
interface AuthorizeNetTransactionRefundOrVoidInterface {

  /**
   * Set the payment method configuration.
   *
   * @param array $paymentMethodConfig
   *   The payment method configuration.
   */
  public function setPaymentMethodConfig(array $paymentMethodConfig);

  /**
   * Refund or void an Authorize.Net transaction.
   *
   * @param string|float|int $transactionId
   *   A transaction ID.
   *
   * @return \net\authorize\api\contract\v1\CreateTransactionResponse
   *   Information about the refund transaction that was just created.
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ActionNotPossibleException
   *   Throws an ActionNotPossibleException if the given transaction cannot be
   *   refunded nor voided.
   */
  public function refundOrVoid($transactionId);

  /**
   * Determine if a given transaction should be refunded.
   *
   * @param \net\authorize\api\contract\v1\TransactionDetailsType $transaction
   *   The transaction to evaluate.
   *
   * @return bool
   *   TRUE if the given transaction should be refunded (not voided).
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   */
  public function shouldRefund(TransactionDetailsType $transaction);

  /**
   * Perform a refund for an earlier transaction.
   *
   * @param string|float|int $transactionId
   *   The transaction ID of the earlier transaction.
   * @param \net\authorize\api\contract\v1\TransactionDetailsType $transaction
   *   The transaction to refund.
   *
   * @return \net\authorize\api\contract\v1\CreateTransactionResponse
   *   Information about the refund transaction that was just created.
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   */
  public function refundTransaction($transactionId, TransactionDetailsType $transaction);

  /**
   * Determine if a given transaction should be voided.
   *
   * @param \net\authorize\api\contract\v1\TransactionDetailsType $transaction
   *   The transaction to evaluate.
   *
   * @return bool
   *   TRUE if the given transaction should be voided (not refunded).
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   */
  public function shouldVoid(TransactionDetailsType $transaction);

  /**
   * Perform a void for an earlier transaction.
   *
   * @param string|float|int $transactionId
   *   The transaction ID of the earlier transaction.
   *
   * @return \net\authorize\api\contract\v1\CreateTransactionResponse
   *   Information about the void transaction that was just created.
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   */
  public function voidTransaction($transactionId);

}
