<?php

namespace Drupal\payment_authnet_accepthosted\Service;

/**
 * Get an Authorize.Net Accept Hosted payment form token.
 */
interface AuthorizeNetAcceptHostedFormTokenFactoryInterface {

  /**
   * Get an Authorize.Net Accept Hosted payment form token.
   *
   * @param string|float|int $amount
   *   The amount of money the transaction should be for.
   *
   * @return string
   *   An Authorize.Net Accept Hosted payment form token.
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   */
  public function getToken($amount);

  /**
   * Set the payment method configuration.
   *
   * @param array $paymentMethodConfig
   *   The payment method configuration.
   */
  public function setPaymentMethodConfig(array $paymentMethodConfig);

}
