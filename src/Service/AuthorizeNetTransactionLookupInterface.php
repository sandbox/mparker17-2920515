<?php

namespace Drupal\payment_authnet_accepthosted\Service;

/**
 * Look up an Authorize.Net transaction.
 */
interface AuthorizeNetTransactionLookupInterface {

  /**
   * Set the payment method configuration.
   *
   * @param array $paymentMethodConfig
   *   The payment method configuration.
   */
  public function setPaymentMethodConfig(array $paymentMethodConfig);

  /**
   * Get details about an Authorize.Net transaction.
   *
   * @param string|float|int $transactionId
   *   A transaction ID.
   *
   * @return \net\authorize\api\contract\v1\TransactionDetailsType
   *   An Authorize.Net Transaction Details object.
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\NullResponseException
   *   Throws a NullResponseException if the Authorize.Net API returned a NULL
   *   response.
   * @throws \Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException
   *   Throws a ResultCodeNotOkException if the Authorize.Net API returned a
   *   response indicating that it did not successfully complete.
   */
  public function getTransactionDetails($transactionId);

}
