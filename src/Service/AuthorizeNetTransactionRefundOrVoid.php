<?php

namespace Drupal\payment_authnet_accepthosted\Service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\payment_authnet_accepthosted\AuthorizeNetV1TransactionCodeAndStatusInterpreter;
use Drupal\payment_authnet_accepthosted\Exception\ActionNotPossibleException;
use Drupal\payment_authnet_accepthosted\Exception\InvalidRefundAmountException;
use Drupal\payment_authnet_accepthosted\Exception\NullResponseException;
use Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1ResultCodes;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1TransactionResponseCodes;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1TransactionTypes;
use net\authorize\api\contract\v1\CreateTransactionRequest;
use net\authorize\api\contract\v1\CreditCardType;
use net\authorize\api\contract\v1\MerchantAuthenticationType;
use net\authorize\api\contract\v1\PaymentType;
use net\authorize\api\contract\v1\TransactionDetailsType;
use net\authorize\api\contract\v1\TransactionRequestType;
use net\authorize\api\controller\CreateTransactionController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Refund or void an Authorize.Net transaction.
 */
class AuthorizeNetTransactionRefundOrVoid implements AuthorizeNetTransactionRefundOrVoidInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The configuration for this payment method.
   *
   * @var array
   */
  protected $paymentMethodConfig;

  /**
   * A service we can use to get information about the transaction.
   *
   * @var \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionLookup
   */
  protected $transactionLookupService;

  /**
   * AuthorizeNetTransactionRefundOrVoid constructor.
   *
   * @param \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionLookup $transactionLookupService
   *   A service we can use to get information about the transaction.
   */
  public function __construct(AuthorizeNetTransactionLookup $transactionLookupService) {
    $this->transactionLookupService = $transactionLookupService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('payment_authnet_accepthosted.transaction_lookup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentMethodConfig(array $paymentMethodConfig) {
    $this->paymentMethodConfig = $paymentMethodConfig;
    $this->transactionLookupService->setPaymentMethodConfig($paymentMethodConfig);
  }

  /**
   * {@inheritdoc}
   */
  public function refundOrVoid($transactionId) {
    /** @var \net\authorize\api\contract\v1\TransactionDetailsType $transaction */
    $transaction = $this->transactionLookupService->getTransactionDetails($transactionId);

    // If we should refund the transaction, do that.
    if ($this->shouldRefund($transaction)) {
      return $this->refundTransaction($transactionId, $transaction);
    }
    // If we should void the transaction, do that.
    elseif ($this->shouldVoid($transaction)) {
      return $this->voidTransaction($transactionId);
    }

    // If we get here, then we shouldn't have tried refunding or voiding: throw
    // an error.
    throw new ActionNotPossibleException($this->t('Cannot refund or void transaction with Authorize.Net ID @transactionId.', [
      '@transactionId' => $transactionId,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function shouldRefund(TransactionDetailsType $transaction) {
    $responseCode = $transaction->getResponseCode();
    $transactionStatus = $transaction->getTransactionStatus();

    return AuthorizeNetV1TransactionCodeAndStatusInterpreter::isRefundable($responseCode, $transactionStatus);
  }

  /**
   * {@inheritdoc}
   */
  public function refundTransaction($transactionId, TransactionDetailsType $transaction) {
    $amount = $this->determineRefundAmount($transaction);

    $request_authentication = (new MerchantAuthenticationType())
      ->setName($this->paymentMethodConfig['merchantAuthentication_name'])
      ->setTransactionKey($this->paymentMethodConfig['merchantAuthentication_transactionKey']);

    $unique_reference_id = 'ref' . time();

    $credit_card = (new CreditCardType())
      ->setCardNumber($transaction->getPayment()->getCreditCard()->getCardNumber())
      ->setExpirationDate($transaction->getPayment()->getCreditCard()->getExpirationDate());

    $payment = (new PaymentType())
      ->setCreditCard($credit_card);

    $transaction_request_type = (new TransactionRequestType())
      ->setTransactionType(AuthorizeNetV1TransactionTypes::REFUND_TRANSACTION)
      ->setAmount($amount)
      ->setPayment($payment)
      ->setRefTransId($transactionId);

    /** @var \net\authorize\api\contract\v1\CreateTransactionRequest $request */
    $request = (new CreateTransactionRequest())
      ->setMerchantAuthentication($request_authentication)
      ->setRefId($unique_reference_id)
      ->setTransactionRequest($transaction_request_type);

    $controller = new CreateTransactionController($request);
    /** @var \net\authorize\api\contract\v1\CreateTransactionResponse $response */
    $response = $controller->executeWithApiResponse($this->paymentMethodConfig['environment']);

    if (is_null($response)) {
      throw new NullResponseException('Authorize.Net API returned a NULL response.');
    }
    if ($response->getMessages()->getResultCode() !== AuthorizeNetV1ResultCodes::OK) {
      $messages = [];
      foreach ($response->getMessages()->getMessage() as $message) {
        $messages[] = $this->t('Key: @key, Text: @text.', [
          '@key' => $message->getCode(),
          '@text' => $message->getText(),
        ]);
      }

      throw new ResultCodeNotOkException('Authorize.Net API returned a non-Ok response.',
        0,
        $response->getMessages()->getResultCode(),
        $messages
      );
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldVoid(TransactionDetailsType $transaction) {
    $responseCode = $transaction->getResponseCode();
    $transactionStatus = $transaction->getTransactionStatus();

    return AuthorizeNetV1TransactionCodeAndStatusInterpreter::isVoidable($responseCode, $transactionStatus);
  }

  /**
   * {@inheritdoc}
   */
  public function voidTransaction($transactionId) {
    $request_authentication = (new MerchantAuthenticationType())
      ->setName($this->paymentMethodConfig['merchantAuthentication_name'])
      ->setTransactionKey($this->paymentMethodConfig['merchantAuthentication_transactionKey']);

    $unique_reference_id = 'ref' . time();

    $transaction_request_type = (new TransactionRequestType())
      ->setTransactionType(AuthorizeNetV1TransactionTypes::VOID_TRANSACTION)
      ->setRefTransId($transactionId);

    $request = (new CreateTransactionRequest())
      ->setMerchantAuthentication($request_authentication)
      ->setRefId($unique_reference_id)
      ->setTransactionRequest($transaction_request_type);

    $controller = new CreateTransactionController($request);
    /** @var \net\authorize\api\contract\v1\CreateTransactionResponse $response */
    $response = $controller->executeWithApiResponse($this->paymentMethodConfig['environment']);

    if (is_null($response)) {
      throw new NullResponseException('Authorize.Net API returned a NULL response.');
    }
    if ($response->getMessages()->getResultCode() !== AuthorizeNetV1ResultCodes::OK) {
      $messages = [];
      foreach ($response->getMessages()->getMessage() as $message) {
        $messages[] = $this->t('Key: @key, Text: @text.', [
          '@key' => $message->getCode(),
          '@text' => $message->getText(),
        ]);
      }

      throw new ResultCodeNotOkException('Authorize.Net API returned a non-Ok response.',
        0,
        $response->getMessages()->getResultCode(),
        $messages
      );
    }

    return $response;
  }

  /**
   * Determine how much money to refund.
   *
   * @param \net\authorize\api\contract\v1\TransactionDetailsType $original_transaction
   *   The original transaction that we will be issuing a refund for.
   *
   * @return float
   *   The amount to refund.
   *
   * @throws \Drupal\payment_authnet_accepthosted\Exception\InvalidRefundAmountException
   *   Throws an invalid refund amount exception if the original transaction's
   *   amount settled was not set; or it was less than or equal to zero.
   */
  public function determineRefundAmount(TransactionDetailsType $original_transaction) {
    $amount_settled = $original_transaction->getSettleAmount();
    $response_code = $original_transaction->getResponseCode();

    // The amount settled can be non-zero for declined, errored, or held
    // transactions; but no money has been transferred, so we can't refund them!
    if ($response_code !== AuthorizeNetV1TransactionResponseCodes::APPROVED) {
      throw new ActionNotPossibleException($this->t('Told to refund a declined, error-ed, or held transaction (response code %response_code), but money has not been transferred for transactions in those states.', [
        '%response_code' => $response_code,
      ]));
    }
    // If there is not an amount settled, then either Authorize.Net's API is
    // returning an error (so the amount is indeterminate) or the transaction is
    // not yet settled (this shouldn't happen if we've already tested for
    // voiding; but we don't want to take that chance).
    elseif (is_null($amount_settled)) {
      throw new InvalidRefundAmountException($this->t('Told to refund transaction %transaction_id, but it did not have an amount settled.', [
        '%transaction_id' => $original_transaction->getTransId(),
      ]));
    }
    // If the amount settled is zero, then the transaction was already voided.
    // If the amount was less than zero, then we've been asked to operate on a
    // refund transaction somehow (this shouldn't happen but we don't want to
    // take that chance).
    elseif ($amount_settled <= 0.0) {
      throw new InvalidRefundAmountException($this->t('Told to refund transaction %transaction_id, but it the amount settled (%amount) was less than or equal to 0.0.', [
        '%transaction_id' => $original_transaction->getTransId(),
        '%amount' => $amount_settled,
      ]));
    }

    // If we get here, then everything worked, so return the amount settled by
    // the previous transaction.
    return $amount_settled;
  }

}
