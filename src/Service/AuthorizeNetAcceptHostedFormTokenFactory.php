<?php

namespace Drupal\payment_authnet_accepthosted\Service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\payment_authnet_accepthosted\Exception\NullResponseException;
use Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1ResultCodes;
use net\authorize\api\contract\v1\GetHostedPaymentPageRequest;
use net\authorize\api\contract\v1\MerchantAuthenticationType;
use net\authorize\api\contract\v1\SettingType;
use net\authorize\api\contract\v1\TransactionRequestType;
use net\authorize\api\controller\GetHostedPaymentPageController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Get an Authorize.Net Accept Hosted payment form token.
 */
class AuthorizeNetAcceptHostedFormTokenFactory implements AuthorizeNetAcceptHostedFormTokenFactoryInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The configuration for this payment method.
   *
   * @var array
   */
  protected $paymentMethodConfig;

  /**
   * AuthorizeNetAcceptHostedFormTokenFactory constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack that controls the lifecycle of requests.
   */
  public function __construct(RequestStack $requestStack) {
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function setPaymentMethodConfig(array $paymentMethodConfig) {
    $this->paymentMethodConfig = $paymentMethodConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken($amount) {
    $amount = (float) $amount;

    $request_authentication = (new MerchantAuthenticationType())
      ->setName($this->paymentMethodConfig['merchantAuthentication_name'])
      ->setTransactionKey($this->paymentMethodConfig['merchantAuthentication_transactionKey']);

    $unique_reference_id = 'ref' . time();

    $transaction_request = (new TransactionRequestType())
      ->setTransactionType($this->paymentMethodConfig['transactionRequest_transactionType'])
      ->setAmount($amount);

    $hosted_payment_page_request = (new GetHostedPaymentPageRequest())
      ->setMerchantAuthentication($request_authentication)
      ->setRefId($unique_reference_id)
      ->setTransactionRequest($transaction_request);

    foreach ($this->buildSettings() as $setting) {
      $hosted_payment_page_request->addToHostedPaymentSettings($setting);
    }

    $controller = new GetHostedPaymentPageController($hosted_payment_page_request);
    /** @var \net\authorize\api\contract\v1\GetHostedPaymentPageResponse $response */
    $response = $controller->executeWithApiResponse($this->paymentMethodConfig['environment']);

    if (is_null($response)) {
      throw new NullResponseException('Authorize.Net API returned a NULL response.');
    }
    if ($response->getMessages()->getResultCode() !== AuthorizeNetV1ResultCodes::OK) {
      $messages = [];
      foreach ($response->getMessages()->getMessage() as $message) {
        $messages[] = $this->t('Key: @key, Text: @text.', [
          '@key' => $message->getCode(),
          '@text' => $message->getText(),
        ]);
      }

      throw new ResultCodeNotOkException('Authorize.Net API returned a non-Ok response.',
        0,
        $response->getMessages()->getResultCode(),
        $messages
      );
    }

    return $response->getToken();
  }

  /**
   * Build the settings we need for this hosted payment page request.
   *
   * @return \net\authorize\api\contract\v1\SettingType[]
   *   An array of Settings to add to the hosted payment page request.
   */
  protected function buildSettings() {
    $output = [];

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentReturnOptions')
      ->setSettingValue(json_encode([
        'showReceipt' => FALSE,
        'url' => (string) Url::fromRoute('payment.authnet_accepthosted.continue_callback')->setAbsolute()->toString(),
        'urlText' => (string) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentButtonOptions_text'],
        'cancelUrl' => (string) Url::fromRoute('payment.authnet_accepthosted.cancel_callback')->setAbsolute()->toString(),
        'cancelUrlText' => (string) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentButtonOptions')
      ->setSettingValue(json_encode([
        'text' => (string) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentButtonOptions_text'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentStyleOptions')
      ->setSettingValue(json_encode([
        'bgColor' => (string) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentStyleOptions_bgColor'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentPaymentOptions')
      ->setSettingValue(json_encode([
        'cardCodeRequired' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentSecurityOptions')
      ->setSettingValue(json_encode([
        'captcha' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentSecurityOptions_captcha'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentShippingAddressOptions')
      ->setSettingValue(json_encode([
        'show' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentShippingAddressOptions_show'],
        'required' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentShippingAddressOptions_required'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentBillingAddressOptions')
      ->setSettingValue(json_encode([
        'show' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentBillingAddressOptions_show'],
        'required' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentBillingAddressOptions_required'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentCustomerOptions')
      ->setSettingValue(json_encode([
        'showEmail' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail'],
        'requiredEmail' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentOrderOptions')
      ->setSettingValue(json_encode([
        'show' => (bool) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentOrderOptions_show'],
        'merchantName' => (string) $this->paymentMethodConfig['hostedPaymentSettings_hostedPaymentOrderOptions_merchantName'],
      ]));

    $output[] = (new SettingType())
      ->setSettingName('hostedPaymentIFrameCommunicatorUrl')
      ->setSettingValue(json_encode([
        'url' => $this->getAbsoluteUrlToIframeCommunicator(),
      ]));

    return $output;
  }

  /**
   * Get the absolute URL to the IFrameCommunicator page.
   *
   * We need to pass this to the Accept Hosted payment form so that it can pass
   * messages back to the page that the Accept Hosted form is embedded in. See
   * js/accept_hosted_communication_handler.js for more information.
   *
   * Based upon file_url_transform_relative().
   *
   * @return string
   *   The absolute URL, including scheme, host, base path, and module path, to
   *   the html/IFrameCommunicator.html file.
   *
   * @see file_url_transform_relative()
   */
  protected function getAbsoluteUrlToIframeCommunicator() {
    $scheme_and_http_host = $this->request->getSchemeAndHttpHost();
    $current_module_path = (string) base_path()
      . drupal_get_path('module', 'payment_authnet_accepthosted');

    return $scheme_and_http_host . $current_module_path . '/html/IFrameCommunicator.html';
  }

}
