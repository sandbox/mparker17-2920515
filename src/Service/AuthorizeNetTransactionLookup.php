<?php

namespace Drupal\payment_authnet_accepthosted\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\payment_authnet_accepthosted\Exception\NullResponseException;
use Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1ResultCodes;
use net\authorize\api\contract\v1\GetTransactionDetailsRequest;
use net\authorize\api\contract\v1\MerchantAuthenticationType;
use net\authorize\api\controller\GetTransactionDetailsController;

/**
 * Look up an Authorize.Net transaction.
 */
class AuthorizeNetTransactionLookup implements AuthorizeNetTransactionLookupInterface {

  use StringTranslationTrait;

  /**
   * The configuration for this payment method.
   *
   * @var array
   */
  protected $paymentMethodConfig;

  /**
   * {@inheritdoc}
   */
  public function setPaymentMethodConfig(array $paymentMethodConfig) {
    $this->paymentMethodConfig = $paymentMethodConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionDetails($transactionId) {
    $request_authentication = (new MerchantAuthenticationType())
      ->setName($this->paymentMethodConfig['merchantAuthentication_name'])
      ->setTransactionKey($this->paymentMethodConfig['merchantAuthentication_transactionKey']);

    $transaction_details_request = (new GetTransactionDetailsRequest())
      ->setMerchantAuthentication($request_authentication)
      ->setTransId($transactionId);

    $controller = new GetTransactionDetailsController($transaction_details_request);
    /** @var \net\authorize\api\contract\v1\GetTransactionDetailsResponse $response */
    $response = $controller->executeWithApiResponse($this->paymentMethodConfig['environment']);

    if (is_null($response)) {
      throw new NullResponseException('Authorize.Net API returned a NULL response.');
    }
    if ($response->getMessages()->getResultCode() !== AuthorizeNetV1ResultCodes::OK) {
      $messages = [];
      foreach ($response->getMessages()->getMessage() as $message) {
        $messages[] = $this->t('Key: @key, Text: @text.', [
          '@key' => $message->getCode(),
          '@text' => $message->getText(),
        ]);
      }

      throw new ResultCodeNotOkException('Authorize.Net API returned a non-Ok response.',
        0,
        $response->getMessages()->getResultCode(),
        $messages
      );
    }

    return $response->getTransaction();
  }

}
