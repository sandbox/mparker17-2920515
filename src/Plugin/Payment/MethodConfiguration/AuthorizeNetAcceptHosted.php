<?php

namespace Drupal\payment_authnet_accepthosted\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBase;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1PaymentFormUrls;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1TransactionTypes;
use net\authorize\api\constants\ANetEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the configuration for an Authorize.Net Accept Hosted payment method.
 *
 * @PaymentMethodConfiguration(
 *   id = "payment_authnet_accepthosted",
 *   label = @Translation("Authorize.Net Accept Hosted"),
 *   description = @Translation("A mobile-optimized payment form hosted by Authorize.Net that enables developers to use the Authorize.Net API to submit payment transactions, while maintaining SAQ-A level PCI compliance.")
 * )
 */
class AuthorizeNetAcceptHosted extends PaymentMethodConfigurationBase implements ContainerFactoryPluginInterface {

  /**
   * The default accent color for the Accept Hosted form.
   *
   * Authorize.Net specifies this particular color in their API.
   *
   * @var string
   */
  const DEFAULT_BGCOLOR = '#3f8fcd';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    ModuleHandlerInterface $module_handler
  ) {
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition, $string_translation, $module_handler);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      // Connection settings.
      'environment' => ANetEnvironment::SANDBOX,
      'form_submit_url' => AuthorizeNetV1PaymentFormUrls::SANDBOX,
      'merchantAuthentication_name' => '',
      'merchantAuthentication_transactionKey' => '',

      // Transaction settings.
      'transactionRequest_transactionType' => AuthorizeNetV1TransactionTypes::AUTH_CAPTURE_TRANSACTION,

      // Hosted payment settings.
      'hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired' => FALSE,
      'hostedPaymentSettings_hostedPaymentSecurityOptions_captcha' => FALSE,
      'hostedPaymentSettings_hostedPaymentShippingAddressOptions_show' => FALSE,
      'hostedPaymentSettings_hostedPaymentShippingAddressOptions_required' => FALSE,
      'hostedPaymentSettings_hostedPaymentBillingAddressOptions_show' => TRUE,
      'hostedPaymentSettings_hostedPaymentBillingAddressOptions_required' => FALSE,
      'hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail' => FALSE,
      'hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail' => FALSE,
      'hostedPaymentSettings_hostedPaymentOrderOptions_show' => TRUE,
      'hostedPaymentSettings_hostedPaymentOrderOptions_merchantName' => '',
      'hostedPaymentSettings_hostedPaymentButtonOptions_text' => $this->t('Continue'),
      'hostedPaymentSettings_hostedPaymentStyleOptions_bgColor' => self::DEFAULT_BGCOLOR,
      'hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText' => $this->t('Cancel'),
      'hostedPaymentSettings_hostedPaymentReturnOptions_urlText' => $this->t('Continue'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['connection'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Authorize.Net API connection settings'),
      '#description' => $this->t('You can find these values by logging in to your <a href=":authorize_net_sandbox_url">Authorize.Net sandbox</a>, going to <em>Account</em>, then clicking on <em>Settings</em>.', [
        ':authorize_net_sandbox_url' => 'https://sandbox.authorize.net/',
      ]),
    ];
    $form['connection']['sandbox_mode_note'] = [
      '#type' => 'item',
      '#title' => $this->t('Disable <em>Test mode</em>'),
      '#description' => $this->t('You must switch your Authorize.Net account from Test mode to Live mode in order for this module to work properly, <strong>even if you are not ready to process real money yet</strong>.<br />Transactions are not stored in Test mode, so new transactions are given a reference ID of 0, making it impossible to look up and verify transactions (you will receive "E00040 The record cannot be found." errors). <strong>The <em>Environment</em> settings below determine whether real money is processed.</strong><br />To switch your Authorize.Net account from Test mode to Live mode; on the <em>Settings</em> page, look under <em>Security Settings</em>: <em>General Security Settings</em>, then click <em>Test Mode</em>. Change <em>Transaction Processing</em> from <em>Test</em> to <em>Live</em>.'),
    ];
    $form['connection']['transaction_details_note'] = [
      '#type' => 'item',
      '#title' => $this->t('Enable <em>Transaction Details API</em>'),
      '#description' => $this->t('You will need to enable the Transaction Details API in order for this module to work properly. When this module tries look up and verify transactions, and the Transaction Details API is not enabled, then you will receive "E00011, Text: Access denied. You do not have permissions to call the Transaction Details API." errors.<br />To enable the Transaction Details API; on the <em>Settings</em> page, look under <em>Security Settings</em>: <em>General Security Settings</em>, then click <em>Transaction Details API</em>. Fill out the form to enable the Transaction Details API.'),
    ];
    $form['connection']['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Authorize.Net environment'),
      '#default_value' => $this->configuration['environment'],
      '#required' => TRUE,
      '#options' => [
        ANetEnvironment::SANDBOX => $this->t('Sandbox'),
        ANetEnvironment::PRODUCTION => $this->t('Production'),
      ],
    ];
    $form['connection']['form_submit_url'] = [
      '#type' => 'radios',
      '#title' => $this->t('Form submit URL'),
      '#default_value' => $this->configuration['form_submit_url'],
      '#required' => TRUE,
      '#options' => [
        AuthorizeNetV1PaymentFormUrls::SANDBOX => $this->t('Sandbox'),
        AuthorizeNetV1PaymentFormUrls::PRODUCTION => $this->t('Production'),
      ],
    ];
    $form['connection']['test_mode_note'] = [
      '#type' => 'item',
      '#title' => $this->t('Testing guide'),
      '#description' => $this->t('On the Sandbox environment, transactions will appear to work, but you will not receive actual money! See <a href=":testing_guide_url">the Developer.Authorize.Net Testing Guide</a> for more information, including test data that you can enter to simulate conditions which might occur in the real world.', [
        ':testing_guide_url' => 'https://developer.authorize.net/hello_world/testing_guide/',
      ]),
      '#states' => [
        'visible' => [
          ':input[name$="[environment]"]' => [
            'value' => ANetEnvironment::SANDBOX,
          ],
        ],
      ],
    ];
    $form['connection']['merchantAuthentication_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t("API Login ID"),
      '#default_value' => $this->configuration['merchantAuthentication_name'],
      '#required' => TRUE,
      '#description' => $this->t('To find this value on the <em>Settings</em> page, look under <em>Security Settings</em>: <em>General Security Settings</em>, then click <em>API Credentials & Keys</em>. The API Login ID is listed at the end of the first part of the page.'),
    ];
    $form['connection']['merchantAuthentication_transactionKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Merchant's unique Transaction Key"),
      '#default_value' => $this->configuration['merchantAuthentication_transactionKey'],
      '#required' => TRUE,
      '#description' => $this->t('To find this value on the <em>Settings</em> page, look under <em>Security Settings</em>: <em>General Security Settings</em>, then click <em>API Credentials & Keys</em>. You can obtain a new Transaction Key by filling out the <em>Create New Key(s)</em> form.'),
    ];

    $form['transaction'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Authorize.Net transaction request settings'),
    ];
    $form['transaction']['transactionRequest_transactionType'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type of credit card transaction'),
      '#default_value' => $this->configuration['transactionRequest_transactionType'],
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#options' => [
        AuthorizeNetV1TransactionTypes::AUTH_CAPTURE_TRANSACTION => $this->t('Authorize and capture'),
        AuthorizeNetV1TransactionTypes::AUTH_ONLY_TRANSACTION => $this->t('Authorize only'),
      ],
      '#description' => $this->t('If you choose Authorize only, you will not receive any money until you capture the payment! This module only supports Authorize and capture at this time.'),
    ];

    $form['pay_form_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Authorize.Net hosted payment form options'),
    ];
    $form['pay_form_options']['payment'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Payment information page'),
    ];
    $form['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make the card code a required field'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired'],
    ];

    $form['pay_form_options']['payment']['billing'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Billing address'),
    ];
    $form['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentBillingAddressOptions_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show billing address fields'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentBillingAddressOptions_show'],
    ];
    $form['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentBillingAddressOptions_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make the billing address fields required'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentBillingAddressOptions_required'],
    ];
    $form['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ask the customer for their email address'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail'],
    ];
    $form['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make the customer email address field required'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail'],
    ];

    $form['pay_form_options']['payment']['shipping'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Shipping address'),
    ];
    $form['pay_form_options']['payment']['shipping']['hostedPaymentSettings_hostedPaymentShippingAddressOptions_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show shipping address fields'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentShippingAddressOptions_show'],
    ];
    $form['pay_form_options']['payment']['shipping']['hostedPaymentSettings_hostedPaymentShippingAddressOptions_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make the shipping address fields required'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentShippingAddressOptions_required'],
    ];
    $form['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentSecurityOptions_captcha'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always show a CAPTCHA'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentSecurityOptions_captcha'],
    ];
    $form['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentStyleOptions_bgColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Form accent color'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentStyleOptions_bgColor'],
      '#description' => $this->t('The accent color is used for buttons and field lines.'),
    ];
    $form['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cancel button text'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText'],
      '#description' => $this->t('This button is shown on the first page of the payment form, to cancel payment.'),
    ];

    $form['pay_form_options']['receipt'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Receipt page'),
    ];
    $form['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentButtonOptions_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment button text'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentButtonOptions_text'],
      '#description' => $this->t('This button is shown at the top of the second page of the payment form.'),
    ];
    $form['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentOrderOptions_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the merchant name on the payment form'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentOrderOptions_show'],
    ];
    $form['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentOrderOptions_merchantName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Override the merchant name'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentOrderOptions_merchantName'],
      '#description' => $this->t('This text is shown on the second page of the payment form.'),
      '#placeholder' => $this->t('(use Authorize.Net account name)'),
    ];
    $form['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentReturnOptions_urlText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Return button text'),
      '#default_value' => $this->configuration['hostedPaymentSettings_hostedPaymentReturnOptions_urlText'],
      '#description' => $this->t('This button is shown on the second (receipt) page of the payment form, to return to Drupal.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // No-op.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // Note that the configuration form has $form['#parents'] set, as well as
    // $form['#tree'] = TRUE, so the form state values are in a tree structure.
    // The first few levels are specified in $form['#parents'] and could be
    // anything; this removes those first few levels.
    $parents = $form['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->configuration['environment'] = $values['connection']['environment'];
    $this->configuration['form_submit_url'] = $values['connection']['form_submit_url'];
    $this->configuration['merchantAuthentication_name'] = $values['connection']['merchantAuthentication_name'];
    $this->configuration['merchantAuthentication_transactionKey'] = $values['connection']['merchantAuthentication_transactionKey'];
    $this->configuration['transactionRequest_transactionType'] = $values['transaction']['transactionRequest_transactionType'];
    $this->configuration['hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired'] = $values['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired'];
    $this->configuration['hostedPaymentSettings_hostedPaymentBillingAddressOptions_show'] = $values['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentBillingAddressOptions_show'];
    $this->configuration['hostedPaymentSettings_hostedPaymentBillingAddressOptions_required'] = $values['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentBillingAddressOptions_required'];
    $this->configuration['hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail'] = $values['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail'];
    $this->configuration['hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail'] = $values['pay_form_options']['payment']['billing']['hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail'];
    $this->configuration['hostedPaymentSettings_hostedPaymentShippingAddressOptions_show'] = $values['pay_form_options']['payment']['shipping']['hostedPaymentSettings_hostedPaymentShippingAddressOptions_show'];
    $this->configuration['hostedPaymentSettings_hostedPaymentShippingAddressOptions_required'] = $values['pay_form_options']['payment']['shipping']['hostedPaymentSettings_hostedPaymentShippingAddressOptions_required'];
    $this->configuration['hostedPaymentSettings_hostedPaymentSecurityOptions_captcha'] = $values['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentSecurityOptions_captcha'];
    $this->configuration['hostedPaymentSettings_hostedPaymentStyleOptions_bgColor'] = $values['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentStyleOptions_bgColor'];
    $this->configuration['hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText'] = $values['pay_form_options']['payment']['hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText'];
    $this->configuration['hostedPaymentSettings_hostedPaymentButtonOptions_text'] = $values['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentButtonOptions_text'];
    $this->configuration['hostedPaymentSettings_hostedPaymentOrderOptions_show'] = $values['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentOrderOptions_show'];
    $this->configuration['hostedPaymentSettings_hostedPaymentOrderOptions_merchantName'] = $values['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentOrderOptions_merchantName'];
    $this->configuration['hostedPaymentSettings_hostedPaymentReturnOptions_urlText'] = $values['pay_form_options']['receipt']['hostedPaymentSettings_hostedPaymentReturnOptions_urlText'];
  }

}
