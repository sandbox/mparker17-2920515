<?php

namespace Drupal\payment_authnet_accepthosted\Plugin\Payment\Method;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\OperationResult;
use Drupal\payment\PaymentAwareTrait;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodCapturePaymentInterface;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodInterface;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodRefundPaymentInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_authnet_accepthosted\AuthorizeNetV1TransactionCodeAndStatusInterpreter;
use Drupal\payment_authnet_accepthosted\Exception\ActionNotPossibleException;
use Drupal\payment_authnet_accepthosted\Exception\NotYetImplementedException;
use Drupal\payment_authnet_accepthosted\Exception\NullResponseException;
use Drupal\payment_authnet_accepthosted\Exception\ResultCodeNotOkException;
use Drupal\payment_authnet_accepthosted\Model\AuthorizeNetV1TransactionTypes;
use Drupal\payment_authnet_accepthosted\Model\TransactionUpdateInformation;
use Drupal\payment_authnet_accepthosted\Service\AuthorizeNetAcceptHostedFormTokenFactoryInterface;
use Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionLookup;
use Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionRefundOrVoidInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A payment method that uses the Authorize.Net Accept Hosted payment method.
 *
 * @PaymentMethod(
 *   id = "payment_authnet_accepthosted",
 *   deriver = "Drupal\payment_authnet_accepthosted\Plugin\Payment\Method\AuthorizeNetAcceptHostedDeriver",
 *   operations_provider = "\Drupal\payment_authnet_accepthosted\Plugin\Payment\Method\AuthorizeNetAcceptHostedOperationsProvider"
 * )
 */
class AuthorizeNetAcceptHosted extends PluginBase implements ContainerFactoryPluginInterface, PaymentMethodInterface, PaymentMethodCapturePaymentInterface, PaymentMethodRefundPaymentInterface, PluginFormInterface, CacheableDependencyInterface, ConfigurablePluginInterface {

  /**
   * The field name that the Authorize.Net servers expect the token in.
   *
   * @var string
   */
  const AUTHORIZE_NET_EXPECTED_TOKEN_FIELD_NAME = 'token';

  /**
   * The HTTP verb that the payment form expects us to send the token with.
   *
   * @var string
   */
  const FORM_METHOD = 'post';

  // Provides a default implementation of \Drupal\payment\PaymentAwareInterface.
  use PaymentAwareTrait;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\payment\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * An Authorize.Net Accept Hosted form token factory.
   *
   * @var \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetAcceptHostedFormTokenFactoryInterface
   */
  protected $formTokenFactory;

  /**
   * A transaction lookup service.
   *
   * @var \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionLookup
   */
  protected $transactionLookup;

  /**
   * A logger we can use to log errors.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The payment status manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * A transaction refund service.
   *
   * @var \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionRefundOrVoidInterface
   */
  protected $refunder;

  /**
   * AuthorizeNetAcceptHosted constructor.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed[] $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetAcceptHostedFormTokenFactoryInterface $formTokenFactory
   *   An Authorize.Net Accept Hosted form token factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger we can use to log errors.
   * @param \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionLookup $transactionLookup
   *   A transaction lookup service.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   * @param \Drupal\payment_authnet_accepthosted\Service\AuthorizeNetTransactionRefundOrVoidInterface $refunder
   *   A transaction refund service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    EventDispatcherInterface $event_dispatcher,
    AuthorizeNetAcceptHostedFormTokenFactoryInterface $formTokenFactory,
    LoggerInterface $logger,
    AuthorizeNetTransactionLookup $transactionLookup,
    PaymentStatusManagerInterface $payment_status_manager,
    AuthorizeNetTransactionRefundOrVoidInterface $refunder
  ) {
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eventDispatcher = $event_dispatcher;
    $this->formTokenFactory = $formTokenFactory;
    $this->logger = $logger;
    $this->transactionLookup = $transactionLookup;
    $this->paymentStatusManager = $payment_status_manager;
    $this->refunder = $refunder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('payment.event_dispatcher'),
      $container->get('payment_authnet_accepthosted.form_token_factory'),
      $container->get('logger.channel.payment_authnet_accepthosted'),
      $container->get('payment_authnet_accepthosted.transaction_lookup'),
      $container->get('plugin.manager.payment.status'),
      $container->get('payment_authnet_accepthosted.refunder')
    );
  }

  /* Cacheable dependency interface. */

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    // The default payment method manager caches definitions using the
    // "payment_method" tag.
    return ['payment_method'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /* Configurable plugin interface. */

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // An instance of a payment needs a form token and a submit URL.
    return [
      'sender' => '',
      'transactResponse_transId' => '',
      'transaction_last_successful_update' => 0,
      'refund_transId' => '',
      'refund_responseCode' => '',
      'refund_type' => '',
      'refund_transaction_last_successful_update' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /* Dependent plugin interface. */

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    // Copied from \Drupal\payment\Plugin\Payment\Method\PaymentMethodBase.
    return [];
  }

  /* Plugin form interface. */

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $form_id = Html::getUniqueId('payment_authnet_accepthosted-accept-hosted-payment-form');
    // Set up the element that will contain the configuration form, ensure that
    // the JS crucial for it to run is attached, and add a class so the JS can
    // identify it.
    $elements['accept_hosted'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'payment_authnet_accepthosted-accept-hosted-payment-form',
        'id' => $form_id,
      ],
      '#attached' => [
        'library' => ['payment_authnet_accepthosted/accept_hosted_form'],
      ],
    ];

    try {
      // Find the payment method settings and amount.
      $payment_method_settings = $this->getPluginDefinition();
      $amount = $this->getPayment()->getAmount();

      // Generate a token, find the submit URL, and pass them to the front-end.
      $this->formTokenFactory->setPaymentMethodConfig($payment_method_settings);
      $token = $this->formTokenFactory->getToken($amount);
      $submit_url = $payment_method_settings['form_submit_url'];
      $elements['accept_hosted']['#attached']['drupalSettings']['payment_authnet_accepthosted_accept_hosted_payment_form'][$form_id] = [
        'authorizeNetToken' => $token,
        'authorizeNetSubmitUrl' => $submit_url,
      ];

      // Add fields to collect the data we will get back from Authorize.Net.
      $elements['accept_hosted']['sender'] = [
        '#type' => 'hidden',
        '#default_value' => $this->configuration['sender'],
      ];
      $elements['accept_hosted']['transactResponse'] = [
        '#type' => 'hidden',
        '#default_value' => '',
      ];
    }
    catch (NullResponseException $e) {
      $this->logger->error('Unable to build configuration form for payment %payment_id because Authorize.Net API returned an empty response in @method.', [
        '%payment_id' => $this->getPayment()->id(),
        '@method' => __METHOD__,
      ]);
      $elements['accept_hosted']['error']['#markup'] = $this->t('Payment is not available right now because the Authorize.Net API is unavailable.');
    }
    catch (ResultCodeNotOkException $e) {
      $this->logger->error('Unable to build configuration form for payment %payment_id because Authorize.Net API returned the following @num_errors error(s) in @method: %errors', [
        '%payment_id' => $this->getPayment()->id(),
        '@num_errors' => count($e->getMessages()),
        '@method' => __METHOD__,
        '%errors' => $e->getMessage(),
      ]);
      $elements['accept_hosted']['error']['#markup'] = $this->t('Payment is not available right now because the Authorize.Net API returned an error.');
    }
    catch (\Exception $e) {
      $this->logger->error('Unable to build configuration form for payment %payment_id because an error of type @type with message @message was caught in @method.', [
        '%payment_id' => $this->getPayment()->id(),
        '@type' => get_class($e),
        '@message' => $e->getMessage(),
        '@method' => __METHOD__,
      ]);
      $elements['accept_hosted']['error']['#markup'] = $this->t('Payment is not available right now because an unexpected error occurred.');
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Note that the configuration form has $form['#parents'] set, as well as
    // $form['#tree'] = TRUE, so the form state values are in a tree structure.
    // The first few levels are specified in $form['#parents'] and could be
    // anything; this removes those first few levels.
    $parents = $form['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    // If the transactResponse is empty or we cant parse it as a JSON string,
    // then we can't determine whether the payment went through or not.
    $response = $values['accept_hosted']['transactResponse'];
    if (empty($response) || json_decode($response) === NULL) {
      $this->logger->critical('A user tried paying for payment %payment_id, but Authorize.Net returned the string <code>@response</code>, which the payment method could not interpret as JSON.', [
        '%payment_id' => $this->getPayment()->id(),
        '@response' => $response,
      ]);
      $form_state->setError($form, $this->t('Authorize.Net sent a response that could not be parsed. Please contact the site owner.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Note that the configuration form has $form['#parents'] set, as well as
    // $form['#tree'] = TRUE, so the form state values are in a tree structure.
    // The first few levels are specified in $form['#parents'] and could be
    // anything; this removes those first few levels.
    $parents = $form['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    // Store the sender.
    $this->configuration['sender'] = $values['accept_hosted']['sender'];

    // Now, try to decode the raw transaction reseponse for the data we need.
    $response = json_decode($values['accept_hosted']['transactResponse']);
    $this->configuration['transactResponse_transId'] =
      isset($response->transId) ? $response->transId : '';
  }

  /* Executing (authorizing) payments. */

  /**
   * {@inheritdoc}
   */
  public function executePaymentAccess(AccountInterface $account) {
    if (!$this->getPayment()) {
      throw new \LogicException('Trying to check access for a non-existing payment. A payment must be set through self::setPayment() first.');
    }

    return AccessResult::allowedIf($this->pluginDefinition['active'])
      ->andIf($this->eventDispatcher->executePaymentAccess($this->getPayment(), $this, $account))
      ->andIf(AccessResult::allowedIf($this->getPayment()->getPaymentStatus()->isOrHasAncestor('payment_created')))
      ->addCacheableDependency($this->getPayment())
      ->addCacheTags(['payment_method']);
  }

  /**
   * {@inheritdoc}
   */
  public function executePayment() {
    // Ensure the payment exists.
    if (!$this->getPayment()) {
      throw new \LogicException('Trying to execute a non-existing payment. A payment must be set through self::setPayment() first.');
    }

    // Look up the transaction on the Authorize.Net API, so that we can
    // validate and save a copy of it.
    //
    // We need to do this just in case the end-user did something malicious on
    // the configuration form, e.g.: modifying the settings on page load so a
    // valid transaction goes through on the sandbox API instead of the
    // production API (in this case, the transaction lookup would fail); or
    // modifying the data returned by the Authorize.Net IFrameCommunicator to
    // say that they paid more than they actually did (we ignore the amount
    // passed back to us from the IFrameCommunicator and check the amount
    // reported to us by Authorize.Net).
    try {
      $lookup_result = $this->updateTransactionFromAuthorizeNet($this->configuration['transactResponse_transId']);
      $transaction = $lookup_result->getTransaction();
      $this->configuration['transaction_last_successful_update'] = $lookup_result->getLastUpdate();

      // Notify that we're about to execute payment.
      $this->eventDispatcher->preExecutePayment($this->getPayment());

      // If the amount that Authorize.Net reports the user paid matches what we
      // planned to charge them...
      $amount_expected = $this->getPayment()->getAmount();
      if ((float) $amount_expected === $transaction->getSettleAmount()) {
        // ...then the appropriate status.
        $this->setAppropriateStatus($transaction->getResponseCode(), $transaction->getTransactionStatus());
      }
      // If the amounts do not match, mark the transaction as failed.
      else {
        $payment = $this->getPayment();
        $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_failed'));
        $payment->save();
      }
    }
    catch (NullResponseException $e) {
      $this->logger->error('Unable to execute payment %payment_id because Authorize.Net API returned an empty response in @method.', [
        '%payment_id' => $this->getPayment()->id(),
        '@method' => __METHOD__,
      ]);
    }
    catch (ResultCodeNotOkException $e) {
      $this->logger->error('Unable to execute payment %payment_id because Authorize.Net API returned the following @num_errors error(s) in @method: %errors', [
        '%payment_id' => $this->getPayment()->id(),
        '@num_errors' => count($e->getMessages()),
        '@method' => __METHOD__,
        '%errors' => $e->getMessage(),
      ]);
    }
    catch (\Exception $e) {
      $this->logger->error('Unable to execute payment %payment_id because an error of type @type with message @message was caught in @method.', [
        '%payment_id' => $this->getPayment()->id(),
        '@type' => get_class($e),
        '@message' => $e->getMessage(),
        '@method' => __METHOD__,
      ]);
    }

    return $this->getPaymentExecutionResult();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {
    return new OperationResult();
  }

  /* Capturing payments. */

  /**
   * {@inheritdoc}
   */
  public function capturePaymentAccess(AccountInterface $account) {
    if (!$this->getPayment()) {
      throw new \LogicException('Trying to check access for a non-existing payment. A payment must be set through self::setPayment() first.');
    }

    // Forbidden because capturing Accept Hosted transactions in a separate step
    // has not yet been implemented.
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment() {
    throw new NotYetImplementedException('Capturing Accept Hosted transactions in a separate step is supported by Authorize.Net but have not yet been implemented in the payment_authnet_accepthosted module.');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentCaptureResult() {
    throw new NotYetImplementedException('Capturing Accept Hosted transactions in a separate step is supported by Authorize.Net but have not yet been implemented in the payment_authnet_accepthosted module.');
  }

  /* Refunding payments. */

  /**
   * {@inheritdoc}
   */
  public function refundPaymentAccess(AccountInterface $account) {
    if (!$this->getPayment()) {
      throw new \LogicException('Trying to check access for a non-existing payment. A payment must be set through self::setPayment() first.');
    }

    return AccessResult::allowedIf($this->pluginDefinition['active'])
      ->andIf($this->eventDispatcher->executePaymentAccess($this->getPayment(), $this, $account))
      ->andIf(AccessResult::allowedIf($this->getPayment()->getPaymentStatus()->isOrHasAncestor('payment_pending')))
      ->andIf(AccessResult::allowedIf($this->getPayment()->getPaymentStatus()->isOrHasAncestor('payment_success')))
      ->addCacheableDependency($this->getPayment())
      ->addCacheTags(['payment_method']);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment() {
    // Ensure the payment exists.
    if (!$this->getPayment()) {
      throw new \LogicException('Trying to refund a non-existing payment. A payment must be set through self::setPayment() first.');
    }

    try {
      // Notify that we're about to refund payment.
      $this->eventDispatcher->preRefundPayment($this->getPayment());

      // Perform the refund.
      $this->refunder->setPaymentMethodConfig($this->getPluginDefinition());
      $response = $this->refunder->refundOrVoid($this->configuration['transactResponse_transId']);

      // Store data about the refund.
      $transaction_response = $response->getTransactionResponse();
      $this->configuration['refund_transId'] = $transaction_response->getTransId();
      $this->configuration['refund_responseCode'] = $transaction_response->getResponseCode();

      // If the original transaction is the same as the refund transaction ID,
      // then a void occurred. We can update the transaction and re-set the
      // Payment status using data from the original transaction. In either
      // case, record the type of refund that occurred.
      if ($this->configuration['transactResponse_transId'] === $this->configuration['refund_transId']) {
        $this->configuration['refund_type'] = AuthorizeNetV1TransactionTypes::VOID_TRANSACTION;

        $lookup_result = $this->updateTransactionFromAuthorizeNet($this->configuration['transactResponse_transId']);
        $this->configuration['transaction_last_successful_update'] = $lookup_result->getLastUpdate();
      }
      // If the original transaction ID differs from the refund transaction ID,
      // then a refund occurred. We need to update the transaction and re-set
      // the Payment status based on the refund transaction, not the original.
      else {
        $this->configuration['refund_type'] = AuthorizeNetV1TransactionTypes::REFUND_TRANSACTION;

        $lookup_result = $this->updateTransactionFromAuthorizeNet($this->configuration['refund_transId']);
        $this->configuration['refund_transaction_last_successful_update'] = $lookup_result->getLastUpdate();
      }

      // Now update the transaction and re-set the status.
      $transaction = $lookup_result->getTransaction();
      $this->configuration['transaction_last_successful_update'] = $lookup_result->getLastUpdate();
      $this->setAppropriateStatus($transaction->getResponseCode(), $transaction->getTransactionStatus());
    }
    catch (NullResponseException $e) {
      $this->logger->error('Unable to refund payment %payment_id because Authorize.Net API returned an empty response in @method.', [
        '%payment_id' => $this->getPayment()->id(),
        '@method' => __METHOD__,
      ]);
    }
    catch (ResultCodeNotOkException $e) {
      $this->logger->error('Unable to refund payment %payment_id because Authorize.Net API returned the following @num_errors error(s) in @method: %errors', [
        '%payment_id' => $this->getPayment()->id(),
        '@num_errors' => count($e->getMessages()),
        '@method' => __METHOD__,
        '%errors' => $e->getMessage(),
      ]);
    }
    catch (ActionNotPossibleException $e) {
      $this->logger->error("Unable to refund payment %payment_id in @method because Authorize.Net already changed its status to a refunded or error state. Please check Authorize.Net's control panel.", [
        '%payment_id' => $this->getPayment()->id(),
        '@method' => __METHOD__,
      ]);
      drupal_set_message($this->t("Unable to refund payment %payment_id because Authorize.Net already changed its status to a refunded or error state. Please check Authorize.Net's control panel.", [
        '%payment_id' => $this->getPayment()->id(),
      ]), 'error');
    }
    catch (\Exception $e) {
      $this->logger->error('Unable to refund payment %payment_id because an error of type @type with message @message was caught in @method.', [
        '%payment_id' => $this->getPayment()->id(),
        '@type' => get_class($e),
        '@message' => $e->getMessage(),
        '@method' => __METHOD__,
      ]);
    }

    return $this->getPaymentRefundResult();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentRefundResult() {
    return new OperationResult();
  }

  /* Helper functions. */

  /**
   * Given an Authorize.Net response code and status, set the payment status.
   *
   * @param string|int $responseCode
   *   An Authorize.Net response code.
   * @param string $transactionStatus
   *   An Authorize.Net transaction status.
   */
  protected function setAppropriateStatus($responseCode, $transactionStatus) {
    $payment_id = $this->payment->id();

    // Assume the status is "unknown" unless we can determine otherwise.
    $payment_status = $this->paymentStatusManager->createInstance('payment_unknown');

    // If we successfully executed the payment, give it the "pending" status.
    if (AuthorizeNetV1TransactionCodeAndStatusInterpreter::inSuccessfulExecuteState($responseCode, $transactionStatus)) {
      $payment_status = $this->paymentStatusManager->createInstance('payment_pending');

      $this->logger->info('Payment %payment_id executed (authorized) successfully through Authorize.Net Accept Hosted (response code was %responseCode and transaction status was %transactionStatus).', [
        '%payment_id' => $payment_id,
        '%responseCode' => $responseCode,
        '%transactionStatus' => $transactionStatus,
      ]);
    }
    // If we successfully captured the payment, give it the "success" status.
    elseif (AuthorizeNetV1TransactionCodeAndStatusInterpreter::inSuccessfulCaptureState($responseCode, $transactionStatus)) {
      $payment_status = $this->paymentStatusManager->createInstance('payment_success');

      $this->logger->info('Payment %payment_id captured successfully through Authorize.Net Accept Hosted (response code was %responseCode and transaction status was %transactionStatus).', [
        '%payment_id' => $payment_id,
        '%responseCode' => $responseCode,
        '%transactionStatus' => $transactionStatus,
      ]);
    }
    // If we successfully refunded the payment, give it the "refunded" status.
    elseif (AuthorizeNetV1TransactionCodeAndStatusInterpreter::inSuccessfulRefundState($responseCode, $transactionStatus)) {
      $payment_status = $this->paymentStatusManager->createInstance('payment_refunded');

      $this->logger->info('Payment %payment_id refunded or voided successfully through Authorize.Net Accept Hosted (response code was %responseCode and transaction status was %transactionStatus).', [
        '%payment_id' => $payment_id,
        '%responseCode' => $responseCode,
        '%transactionStatus' => $transactionStatus,
      ]);
    }
    // If the payment was declined, give it the "failed" status.
    elseif (AuthorizeNetV1TransactionCodeAndStatusInterpreter::inDeclinedState($responseCode, $transactionStatus)) {
      $payment_status = $this->paymentStatusManager->createInstance('payment_failed');

      $this->logger->error('Payment %payment_id was declined through Authorize.Net Accept Hosted (response code was %responseCode and transaction status was %transactionStatus).', [
        '%payment_id' => $payment_id,
        '%responseCode' => $responseCode,
        '%transactionStatus' => $transactionStatus,
      ]);
    }
    // If the payment has been held for manual action, give it the "pending"
    // state.
    elseif (AuthorizeNetV1TransactionCodeAndStatusInterpreter::inNeedsFurtherManualActionState($responseCode, $transactionStatus)) {
      $payment_status = $this->paymentStatusManager->createInstance('payment_pending');

      $this->logger->error('Payment %payment_id was held for manual review through Authorize.Net Accept Hosted (response code was %responseCode and transaction status was %transactionStatus).', [
        '%payment_id' => $payment_id,
        '%responseCode' => $responseCode,
        '%transactionStatus' => $transactionStatus,
      ]);
    }
    // If we get here, the status is unknown. Log this.
    else {
      $this->logger->error('Payment %payment_id resulted in an Authorize.Net response code of %responseCode and transaction status of %transactionStatus, which the system was unable to interpret.', [
        '%payment_id' => $payment_id,
        '%responseCode' => $responseCode,
        '%transactionStatus' => $transactionStatus,
      ]);
    }

    // Set the payment status and save.
    $payment = $this->getPayment();
    $payment->setPaymentStatus($payment_status);
    $payment->save();
  }

  /**
   * Get details of the transaction from Authorize.net and save to config.
   *
   * @param string $transaction_id
   *   The ID of the transaction to get.
   *
   * @return \Drupal\payment_authnet_accepthosted\Model\TransactionUpdateInformation
   *   Details of the transaction.
   */
  protected function updateTransactionFromAuthorizeNet($transaction_id) {
    // Perform the lookup.
    $this->transactionLookup->setPaymentMethodConfig($this->getPluginDefinition());
    $transaction = $this->transactionLookup->getTransactionDetails($transaction_id);
    $success_time = time();

    return new TransactionUpdateInformation($transaction, $success_time);
  }

}
