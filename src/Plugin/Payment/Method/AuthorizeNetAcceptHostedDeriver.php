<?php

namespace Drupal\payment_authnet_accepthosted\Plugin\Payment\Method;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derives payment method plugin definitions based on configuration entities.
 */
class AuthorizeNetAcceptHostedDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The payment method configuration manager.
   *
   * @var \Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface
   */
  protected $paymentMethodConfigurationManager;

  /**
   * The payment method configuration storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentMethodConfigurationStorage;

  /**
   * AuthorizeNetAcceptHostedDeriver constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $payment_method_configuration_storage
   *   The payment method configuration storage.
   * @param \Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface $payment_method_configuration_manager
   *   The payment method configuration manager.
   */
  public function __construct(
    EntityStorageInterface $payment_method_configuration_storage,
    PaymentMethodConfigurationManagerInterface $payment_method_configuration_manager
  ) {
    $this->paymentMethodConfigurationStorage = $payment_method_configuration_storage;
    $this->paymentMethodConfigurationManager = $payment_method_configuration_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('payment_method_configuration'),
      $container->get('plugin.manager.payment.method_configuration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\payment\Entity\PaymentMethodConfigurationInterface[] $payment_methods */
    $payment_methods = $this->paymentMethodConfigurationStorage->loadMultiple();
    foreach ($payment_methods as $payment_method) {
      if ($payment_method->getPluginId() === 'payment_authnet_accepthosted') {
        /** @var \Drupal\payment_authnet_accepthosted\Plugin\Payment\MethodConfiguration\AuthorizeNetAcceptHosted $configuration_plugin */
        $configuration_plugin = $this->paymentMethodConfigurationManager->createInstance($payment_method->getPluginId(), $payment_method->getPluginConfiguration());
        $configuration = $configuration_plugin->getConfiguration();
        $this->derivatives[$payment_method->id()] = [
          'id' => $base_plugin_definition['id'] . ':' . $payment_method->id(),
          'label' => $payment_method->label(),
          'active' => $payment_method->status(),
          'message_text' => $configuration_plugin->getMessageText(),
          'message_text_format' => $configuration_plugin->getMessageTextFormat(),
          'environment' => $configuration['environment'],
          'form_submit_url' => $configuration['form_submit_url'],
          'merchantAuthentication_name' => $configuration['merchantAuthentication_name'],
          'merchantAuthentication_transactionKey' => $configuration['merchantAuthentication_transactionKey'],
          'transactionRequest_transactionType' => $configuration['transactionRequest_transactionType'],
          'hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired' => $configuration['hostedPaymentSettings_hostedPaymentPaymentOptions_cardCodeRequired'],
          'hostedPaymentSettings_hostedPaymentSecurityOptions_captcha' => $configuration['hostedPaymentSettings_hostedPaymentSecurityOptions_captcha'],
          'hostedPaymentSettings_hostedPaymentShippingAddressOptions_show' => $configuration['hostedPaymentSettings_hostedPaymentShippingAddressOptions_show'],
          'hostedPaymentSettings_hostedPaymentShippingAddressOptions_required' => $configuration['hostedPaymentSettings_hostedPaymentShippingAddressOptions_required'],
          'hostedPaymentSettings_hostedPaymentBillingAddressOptions_show' => $configuration['hostedPaymentSettings_hostedPaymentBillingAddressOptions_show'],
          'hostedPaymentSettings_hostedPaymentBillingAddressOptions_required' => $configuration['hostedPaymentSettings_hostedPaymentBillingAddressOptions_required'],
          'hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail' => $configuration['hostedPaymentSettings_hostedPaymentCustomerOptions_showEmail'],
          'hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail' => $configuration['hostedPaymentSettings_hostedPaymentCustomerOptions_requiredEmail'],
          'hostedPaymentSettings_hostedPaymentOrderOptions_show' => $configuration['hostedPaymentSettings_hostedPaymentOrderOptions_show'],
          'hostedPaymentSettings_hostedPaymentOrderOptions_merchantName' => $configuration['hostedPaymentSettings_hostedPaymentOrderOptions_merchantName'],
          'hostedPaymentSettings_hostedPaymentButtonOptions_text' => $configuration['hostedPaymentSettings_hostedPaymentButtonOptions_text'],
          'hostedPaymentSettings_hostedPaymentStyleOptions_bgColor' => $configuration['hostedPaymentSettings_hostedPaymentStyleOptions_bgColor'],
          'hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText' => $configuration['hostedPaymentSettings_hostedPaymentReturnOptions_cancelUrlText'],
          'hostedPaymentSettings_hostedPaymentReturnOptions_urlText' => $configuration['hostedPaymentSettings_hostedPaymentReturnOptions_urlText'],
        ] + $base_plugin_definition;
      }
    }

    return $this->derivatives;
  }

}
