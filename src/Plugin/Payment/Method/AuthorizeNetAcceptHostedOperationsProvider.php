<?php

namespace Drupal\payment_authnet_accepthosted\Plugin\Payment\Method;

use Drupal\payment\Plugin\Payment\Method\PaymentMethodConfigurationOperationsProvider;

/**
 * Provides operations for the AuthorizeNetAcceptHosted payment method.
 */
class AuthorizeNetAcceptHostedOperationsProvider extends PaymentMethodConfigurationOperationsProvider {

  /**
   * {@inheritdoc}
   */
  protected function getPaymentMethodConfiguration($plugin_id) {
    list(, $entity_id) = explode(':', $plugin_id, 2);

    return $this->paymentMethodConfigurationStorage->load($entity_id);
  }

}
