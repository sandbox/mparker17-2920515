/**
 * @file
 * Front-end scripts for Drupal integration with the Accept Hosted payment form.
 */
(function ($, _, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.payment_authnet_accepthosted_accept_hosted_payment_form = {
    attach: function (context) {
      // Loop through each payment form that has just been added to the page,
      // providing our progressive enhancements.
      $('.payment_authnet_accepthosted-accept-hosted-payment-form', context).once('payment_authnet_accepthosted_accept_hosted_payment_form').each(function () {
        var iframeId = _.uniqueId('payment-accept-hosted-form-iframe');
        var paymentFormId = $(this).attr('id');
        var paymentFormSettings = drupalSettings.payment_authnet_accepthosted_accept_hosted_payment_form[paymentFormId];
        var $methodConfigForm = $(this).parents('form');
        var $methodConfigFormControls = $('input[type="submit"]', $methodConfigForm);
        var $iframe;
        var $iframeForm;

        // Hide the method configuration form controls until we've received a
        // response from Authorize.Net.
        $methodConfigFormControls.hide();

        // Insert an iframe with a unique ID / name.
        $iframe = $('<iframe>')
          .attr('name', iframeId)
          .attr('id', iframeId)
          .addClass('payment_authnet_accepthosted-accept-hosted-iframe')
          .css({width: '100%', border: 'none'})
          .insertAfter($methodConfigForm);

        // Create a form that submits to Authorize.Net, then submit it into the
        // iframe.
        $iframeForm = $('<form method="post">')
          .attr('action', paymentFormSettings.authorizeNetSubmitUrl)
          .attr('target', iframeId)
          .insertAfter($methodConfigForm);
        $('<input type="hidden" name="token">')
          .attr('value', paymentFormSettings.authorizeNetToken)
          .appendTo($iframeForm);
        $iframeForm.submit();

        // When the communication handler notifies us that the size of the page
        // in the iframe changed, change the height of the iframe to match. This
        // avoids scrollbars inside the iframe.
        $(document).on('authorizeNetAcceptHostedCommunication_resizeWindow', function (event) {
          $iframe.height(event.detail.height);
        });

        // When the communication handler notifies us that a transaction has
        // occurred inside the iframe...
        $(document).on('authorizeNetAcceptHostedCommunication_transactResponse', function (event) {
          // Store the data that Authorize.Net gave us so that we can verify
          // it.
          $('input[name$="[sender]"]', $methodConfigForm).val(event.detail.sender);
          $('input[name$="[transactResponse]"]', $methodConfigForm).val(event.detail.response);

          // Hide the iframe, show the pay button so that the browser sends its
          // name and value, then submit the form by triggering a click event on
          // the pay button. Triggering the correct button is essential for the
          // Payment module.
          $iframe.hide();
          $methodConfigFormControls.show();
          $methodConfigFormControls.trigger('click');
        });
      });
    }
  };

})(jQuery, _, Drupal, drupalSettings);
