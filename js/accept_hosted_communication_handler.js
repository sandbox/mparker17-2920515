/**
 * @file
 * Define a communication handler for the IFrameCommunicator scripts to talk to.
 *
 * When embedding an Authorize.Net Accept Hosted payment form on a page, i.e.:
 * in an iframe, the Same-origin policy allows the page with the iframe
 * ("embeder") to pass information to the page inside the iframe ("embedee"),
 * but does not allow the embedee to pass data back to the embeder.
 *
 * One way around this is for the embedee to embed a page from the same origin
 * as the embeder ("sub-embedee"), and pass that page data to pass back to the
 * original embeder.
 *
 * That is to say, the page which runs this file ("embeder") should contain an
 * <iframe> ("embedee"), which has its own <iframe> ("sub-embedee").
 *
 * This file defines a point of communication for the sub-embedee to send
 * messages back to the embeder.
 *
 * @see https://developer.mozilla.org/docs/Web/Security/Same-origin_policy
 */

/**
 * An object for the IFrameCommunicator scripts to talk to.
 *
 * @type {object}
 */
window.AuthorizeNetAcceptHostedCommunicationHandler = {

  /**
   * Decode a message string into a JSON object of message parameters.
   *
   * @param {string} messageString
   *   The message string to decode.
   *
   * @return {object}
   *   The decoded JSON object of message parameters.
   */
  decodeMessageString: function (messageString) {
    var messageParameters = [];
    var messageStringArray = messageString.split('&');
    var parameterKVPair;
    var i;

    for (i = 0; i < messageStringArray.length; i++) {
      parameterKVPair = messageStringArray[i].split('=');
      messageParameters[parameterKVPair[0]] = decodeURIComponent(parameterKVPair[1]);
    }

    return messageParameters;
  },

  /**
   * Decode the name of a page sending a message from its URL.
   *
   * This essentially removes the protocol, domain, etc. from the message
   * sender, allowing us to focus on whether the payment page, receipt page,
   * etc. sent the message, and not the environment.
   *
   * @param {string} messageSenderUrl
   *   The full URL of the page that sent the message.
   *
   * @return {string}
   *   The name of the page that sent the message.
   */
  findSenderPageName: function (messageSenderUrl) {
    // Authorize.Net Accept Hosted payment form and receipt pages currently have
    // the name of the page in the 5th position. Note counting starts at 0.
    return messageSenderUrl.split('/')[4];
  },

  /**
   * IFrameCommunicator Callback: Dispatch events upon receiving messages.
   *
   * This function is called directly by the JavaScript in the
   * IFrameCommunicator, and passed details about events that page received,
   * which it converts into an Event for this page.
   *
   * @param {object} messageObject
   *   A message object.
   */
  onReceiveCommunication: function (messageObject) {
    var sender = this.findSenderPageName(messageObject.parent);
    var messageData = this.decodeMessageString(messageObject.qstr);
    var authorizeNetAcceptHostedCommunicationEvent;
    var eventName = 'authorizeNetAcceptHostedCommunication_' + messageData.action;

    // Append the sender to the message data.
    messageData.sender = sender;

    // Create and dispatch the event.
    authorizeNetAcceptHostedCommunicationEvent = new CustomEvent(eventName, {
      detail: messageData
    });
    document.dispatchEvent(authorizeNetAcceptHostedCommunicationEvent);
  }

};
